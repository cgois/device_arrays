import numpy as np
import gdspy

import parking_lot
parkingLot = parking_lot.parkingLot

# print('Using gdspy module version ' + gdspy.__version__)

## ------------------------------------------------------------------ ##
##      "createStadium" FUNCTION
## ------------------------------------------------------------------ ##

"""
A mushroom is comprised of the union of a rectangle and a semidisk.
The createMushroom function creates them and performs a union of shapes.

It receives the parameters: width, height, radius, center and layer.
They represent the following quantities:

width (float): the foot rectangle width
height (float): the foot rectangle height
radius (float): the hat semidisk radius
center (float tuple): the mushroom center
layer (int): layer to put the device to

It returns the parameters: mushroom and enclosure.
They represent the following quantities:

mushroom (Cell): the mushroom cavity
enclosure (float tuple): width and height of rectangle enclosing the device
"""

def createMushroom(width, height, radius, center, layer):

    ## Foot width cannot be greater than disk diameter
    while (width > 2 * radius):
        try:
            print "Error: foot width is grater than hat."
            width = float(raw_input("Enter new width value: "))
        except (ValueError, TypeError):
            print "Not a valid number."

    ## ------------------------------------------------------------------ ##
    ##      INTERNAL PARAMETERS (messy)
    ## ------------------------------------------------------------------ ##

    ## Rectangle enclosing mushroom, to ease calculations.
    enclosure = (2 * radius, radius + height)

    ## Rectangle parameters.
    foot_start = (center[0] - width / 2, center[1] - enclosure[1] / 2)
    foot_end = (foot_start[0] + width, foot_start[1] + height)

    ## Circle parameters.
    hat_center = (center[0], foot_end[1])
    final_angle = np.pi

    ## ------------------------------------------------------------------ ##
    ##      CREATE SHAPES
    ## ------------------------------------------------------------------ ##

    ## Create foot with given width and height.
    foot = gdspy.Rectangle(foot_start, foot_end, layer)

    ## Create hat with given parameters.
    hat = gdspy.Round(hat_center, radius, initial_angle=0, 
    final_angle=final_angle, layer=layer)

    ## Declares the list to store shapes to be united, and append shapes.
    primitives = []
    primitives.append(foot)
    primitives.append(hat)

    ## ------------------------------------------------------------------ ##
    ##      UNION AND RETURN
    ## ------------------------------------------------------------------ ##

    ## Lambda expression to make union of shapes.
    union = lambda p1, p2: p1 or p2

    ## Perform union of rectangles and semidisks stored in primitives[].
    mushroom = gdspy.boolean(primitives, union, max_points=199, layer=layer)

    ## Returns mushroom object, which should then be added to a cell obj.
    return mushroom, enclosure


## ------------------------------------------------------------------ ##
##      "mushroomWithPL" FUNCTION
## ------------------------------------------------------------------ ##

"""
This function is used to create a mushroom with parking lot.

It receives the parameters: width, height, radius, center and layer.
They represent the following quantities:

width (float): the foot rectangle width
height (float): the foot rectangle height
radius (float): the hat semidisk radius
center (float tuple): the mushroom center
layer (int): layer to put the device to

It returns the parameters: device_cell and enclosure.
They represent the following quantities:

device_cell (Cell): the stadium cavity with its parking lot
enclosure (float tuple): width and height of enclosing rectangle
"""

def mushroomWithPL(width, height, radius, center, layer):

    ## First we need a cell to add the polygons to.
    device_cell = gdspy.Cell('DEVICES')
    
    ## Call createStadium and save cavity and cavity enclosure.
    cavity, cav_enc = createMushroom(width, height, radius, center, layer)

    ## Call parkingLot and save cavity and parking lot enclosure.
    parking, enclosure = parkingLot(cav_enc[0], cav_enc[1], center, layer)

    ## Add parts of device to device_cell.
    device_cell.add(cavity)
    device_cell.add(parking)

    return device_cell, enclosure


## ------------------------------------------------------------------ ##
##      TESTING
## ------------------------------------------------------------------ ##    

# ## First we need a cell to add the polygons to.
# msh_cell = gdspy.Cell('MUSHROOM')

# ## Then we call createMushroom.
# msh = createMushroom(10, 10, 10, (0, 0),1)

# ## Now we add it to our cell.
# msh_cell.add(msh)

# ## And export/visualize the result.
# gdspy.gds_print('mushroom.gds', unit=1.0e-6, precision=1.0e-9)
# gdspy.LayoutViewer()