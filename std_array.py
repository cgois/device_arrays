## Importing dependencies...
import gdspy
import numpy as np

from stadium import stadiumWithPL
from device_array import deviceArray

## Some test parameters.
width, radius, center, layer = 10, 10, (0, 0), 1

""" 
Here we create stadium with parking lot, then makes a 3x3 array of devices.
stadiumWithPL returns a Cell containing a stadium with its parking lot.
It also returns a tuple (enclosure) with width and height of the rectangle 
which encloses the whole device. This enclosure is used by deviceArray to
calculate the spacing between each device in the array. Finally,
deviceArray returns a Cell with the whole array of devices, which is then
exported to a .gds file.
"""

stadium, enclosure = stadiumWithPL(width, radius, center, layer)
array_cell = deviceArray(stadium, enclosure, 3, 3)

## And export/visualize the result.
gdspy.gds_print('std_array_test.gds', unit=1.0e-6, precision=1.0e-9)
gdspy.LayoutViewer()