import gdspy
import numpy as np

from mushroom import mushroomWithPL
from device_array import deviceArray

## Some test parameters.
width, height, radius, center, layer = 10, 10, 10, (0, 0), 1

"""Here we create mushroom with parking lot, then makes a 3x3 array of devices.
mushroomWithPL returns a Cell containing a mushroom with its parking lot.
It also returns a tuple (enclosure) with width and height of the rectangle 
which encloses the whole device. This enclosure is used by deviceArray to
calculate the spacing between each device in the array. Finally,
deviceArray returns a Cell with the whole array of devices, which is then
exported to a .gds file.
"""

mushroom, enclosure = mushroomWithPL(width, height, radius, center, layer)
array_cell = deviceArray(mushroom, enclosure, 3, 3)

## And export/visualize the result.
gdspy.gds_print('msh_array_test.gds', unit=1.0e-6, precision=1.0e-9)
gdspy.LayoutViewer()