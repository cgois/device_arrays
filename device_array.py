## TODO: return enclosure

import stadium
import mushroom
import parking_lot

import numpy as np
import gdspy

# print('Using gdspy module version ' + gdspy.__version__)

## ------------------------------------------------------------------ ##
##      "stadiumArray" FUNCTION
## ------------------------------------------------------------------ ##

"""
The deviceArray function creates an array of a given device.

It receives the parameters: device, enclosure, rows, and columns.
They represent the following quantities:

device (Cell): the device to be repeated
enclosure (tuple): rectangle enclosing the device
rows (int): number of lines for the device array
columns (int): number of columns for the device array
"""

def deviceArray(device, enclosure, rows, columns):

    array_cell = gdspy.Cell('ARRAY')

    for row in range(1, rows + 1):
        for column in range(1, columns + 1):

            center = (enclosure[0] * row, enclosure[1] * column)
            array_cell.add(gdspy.CellReference(device, center))

    return array_cell

## ------------------------------------------------------------------ ##
##      TESTING FOR STADIUM
## ------------------------------------------------------------------ ## 

# width = 10
# radius = 10
# center = (0, 0)
# layer = 1

# dev_cell, enc = stadium.stadiumWithPL(width, radius, center, layer)
# array_cell = deviceArray(dev_cell, enc, 3, 3)

# ## And export/visualize the result.
# gdspy.gds_print('std_array_test.gds', unit=1.0e-6, precision=1.0e-9)
# gdspy.LayoutViewer()

## ------------------------------------------------------------------ ##
##      TESTING FOR MUSHROOM
## ------------------------------------------------------------------ ## 

# width = 10
# height = 10
# radius = 10
# center = (0, 0)
# layer = 1

# dev_cell, enc = mushroom.mushroomWithPL(width, height, radius, center, layer)
# array_cell = deviceArray(dev_cell, enc, 3, 3)

# ## And export/visualize the result.
# gdspy.gds_print('std_array_test.gds', unit=1.0e-6, precision=1.0e-9)
# gdspy.LayoutViewer()