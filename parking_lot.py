import gdspy

# print('Using gdspy module version ' + gdspy.__version__)

## ------------------------------------------------------------------ ##
##      "parkingLot" FUNCTION
## ------------------------------------------------------------------ ##

"""
The parkingLot function creates an outer and an inner rectangle, and then
subtracts the inner from the outer, leaving us with a centered empty space
where the device should be placed.

It receives the parameters: width, height, center and layer.
They represent the following quantities:

width (float): the open area width (= inner rectangle width)
height (float): the open area height (= inner rectangle height)
center (float tuple): the parking lot center (= inner rectangle center)
layer (int): layer to put the parking lot to

It returns the parameters: parking_lot and enclosure.
They represent the following quantities:

parking_lot (Cell): the parking lot object
enclosure (float tuple): width and height of rectangle enclosing the device
"""

def parkingLot(width, height, center, layer):

    ## ------------------------------------------------------------------ ##
    ##      INTERNAL PARAMETERS
    ## ------------------------------------------------------------------ ##
    
    ## Thickness of upper and lower strips of the parking lot.
    thickness = 5
    
    ## Separation width between consecutive parking lots.
    separation = 10
    
    ## Gap between end of device and beginning of parking lot.
    gap = 10
    
    ## Update width and gap values.
    width += 2 * gap
    height += 2 * gap

    ## Rectangle enclosing parking lot, to ease calculations.
    enclosure = (width + 2 * separation, height + 2 * thickness)

    ## The gdspy.Rectangle polygon receives diagonally opposing vertices.
    ## The following variables calculate them based on parkingLot parameters.
    inner_lower_vertex = (center[0] - width / 2, center[1] - height / 2)
    inner_upper_vertex = (center[0] + width / 2, center[1] + height / 2)

    outer_lower_vertex = (inner_lower_vertex[0] - separation, 
        inner_lower_vertex[1] - thickness)
    outer_upper_vertex = (inner_upper_vertex[0] + separation,
        inner_upper_vertex[1] + thickness)

    ## ------------------------------------------------------------------ ##
    ##      CREATE RECTANGLES
    ## ------------------------------------------------------------------ ##

    ## Creating rectangles based on internal parameters
    inner_rec = gdspy.Rectangle(inner_lower_vertex, inner_upper_vertex, layer)
    outer_rec = gdspy.Rectangle(outer_lower_vertex, outer_upper_vertex, layer)

    ## We'll store both Rectangle objects in a list for subtracting them.
    rectangles = []
    rectangles.append(inner_rec)
    rectangles.append(outer_rec)

    ## ------------------------------------------------------------------ ##
    ##      SUBTRACTION AND RETURN
    ## ------------------------------------------------------------------ ##

    ## Lambda expression to subtract shapes.
    subtraction = lambda p1, p2: p2 and not p1

    ## Perform subtraction of inner from outer rectangle, stored in primites[].
    parking_lot = gdspy.boolean(rectangles, subtraction, max_points=199, layer=1)

    ## Returns parking_lot object, which should then be added to some Cell.
    return parking_lot, enclosure


## ------------------------------------------------------------------ ##
##      TESTING
## ------------------------------------------------------------------ ##    

# ## First we need a cell to add the polygons to.
# parking_cell = gdspy.Cell('PARKING')

# ## Then we call parkingLot.
# parking_lot = parkingLot(10, 10, (5, 5), 1)

# ## Now we add it to our cell.
# parking_cell.add(parking_lot)

# ## And export/visualize the result.
# gdspy.gds_print('parking_lot.gds', unit=1.0e-6, precision=1.0e-9)
# gdspy.LayoutViewer()