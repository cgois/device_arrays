import numpy as np
import gdspy

import parking_lot
parkingLot = parking_lot.parkingLot

# print('Using gdspy module version ' + gdspy.__version__)

## ------------------------------------------------------------------ ##
##      "createStadium" FUNCTION
## ------------------------------------------------------------------ ##

"""
A stadium is comprised of the union of a rectangle and two semidisks.
The createStadium function creates them and performs a union of shapes.

It receives the parameters: width, radius, center and layer.
They represent the following quantities:

width (float): the center rectangle's width
radius (float): the semidisks' radius
center (float tuple): the stadium center
layer (int): layer to put the device to

It returns the parameters: stadium and enclosure.
They represent the following quantities:

stadium (Cell): the stadium cavity
enclosure (float tuple): width and height of rectangle enclosing the device
"""

def createStadium(width, radius, center, layer):

    ## ------------------------------------------------------------------ ##
    ##      INTERNAL PARAMETERS
    ## ------------------------------------------------------------------ ##

    ## Rectangle parameters.
    height = 2 * radius
    rectangle_start = (center[0] - width / 2, center[1] - height / 2)
    rectangle_end = (center[0] + width / 2, center[1] + height / 2)

    ## Circle parameters.
    disk1_center = (rectangle_start[0], rectangle_start[1] + height / 2)
    disk2_center = (rectangle_end[0], rectangle_end[1] - height / 2)
    angle = np.pi / 2

    ## ------------------------------------------------------------------ ##
    ##      CREATE SHAPES
    ## ------------------------------------------------------------------ ##

    ## Rectangle enclosing mushroom, to ease parking lot creation.
    enclosure = (width + 2 * radius, 2 * radius)

    ## Create rectangle with given width.
    rectangle = gdspy.Rectangle(rectangle_start, rectangle_end, layer)

    ## Create both semidisks, and rotates the first.
    left_disk = gdspy.Round(disk1_center, radius, initial_angle=angle, 
        final_angle=-angle, layer=layer).rotate(np.pi, center=disk1_center)
    
    right_disk = gdspy.Round(disk2_center, radius, initial_angle=angle, 
        final_angle=-angle, layer=layer)

    ## Declares the list to store shapes to be united, and append shapes.
    primitives = []
    primitives.append(rectangle)
    primitives.append(left_disk)
    primitives.append(right_disk)

    ## ------------------------------------------------------------------ ##
    ##      UNION AND RETURN
    ## ------------------------------------------------------------------ ##

    ## Lambda expression to make union of shapes.
    union = lambda p1, p2, p3: p1 or p2 or p3

    ## Perform union of rectangles and semidisks stored in primitives[].
    stadium = gdspy.boolean(primitives, union, max_points=199, layer=layer)

    ## Returns stadium object, which should then be added to a cell obj.
    ## Returns enclosure tuple, which can be used as paramter for parkingLot
    return stadium, enclosure


## ------------------------------------------------------------------ ##
##      "stadiumWithPL" FUNCTION
## ------------------------------------------------------------------ ##

"""
This function is used to create a stadium with parking lot.

It receives the parameters: width, radius, center and layer.
They represent the following quantities:

width (float): the center rectangle's width
radius (float): the semidisks' radius
center (float tuple): the stadium center
layer (int): layer to put the device to

It returns the parameters: device_cell and enclosure.
They represent the following quantities:

device_cell (Cell): the stadium cavity with its parking lot
enclosure (float tuple): width and height of enclosing rectangle
"""

def stadiumWithPL(width, radius, center, layer):

    ## First we need a cell to add the polygons to.
    device_cell = gdspy.Cell('DEVICES')
    
    ## Call createStadium and save cavity and cavity enclosure.
    cavity, cav_enc = createStadium(width, radius, center, layer)

    ## Call parkingLot and save cavity and parking lot enclosure.
    parking, enclosure = parkingLot(cav_enc[0], 
        cav_enc[1], center, layer)

    ## Add parts of device to device_cell.
    device_cell.add(cavity)
    device_cell.add(parking)

    return device_cell, enclosure

## ------------------------------------------------------------------ ##
##      TESTING
## ------------------------------------------------------------------ ##    

# ## First we need a cell to add the polygons to.
# stadium_cell = gdspy.Cell('STADIUM')

# ## Then we call createStadium.
# stadium = createStadium(10, 10, (5, 10), 1)

# ## Now we add it to our cell.
# stadium_cell.add(stadium)

# ## And export/visualize the result.
# gdspy.gds_print('stadium.gds', unit=1.0e-6, precision=1.0e-9)
# gdspy.LayoutViewer()